#include <stdio.h>
#include <stdlib.h>

#define MAX_STEPS 256
#define FALSE 0
#define TRUE 1

int iterationsAtPoint(double cx, double cy);
double exponent(double base, double exponent);
double scaleCoord (double coord, int zoom);
double scale (double v, int zoom);

int main(int argc, char *argv[]){

    double x = 0;
    double y = 0;

    double cx;
    double cy;

    while (y < 512){
        while (x < 512){
            cx = scale(x, 0);
            cy = scale(y, 0);
            if (iterationsAtPoint(cx, cy) > 256){
                printf("*");
            } else {
                printf(" ");
            }

            if (x == 512){
                printf("\n");
            }
            x++;
        }
        x = 0;
        y++;
    }

    return EXIT_SUCCESS;
}

int iterationsAtPoint(double cx, double cy) {
    int escapeLoop = FALSE;
   double curX, curY;
   double tempCurX, tempCurY;
   int repetitionCounter;
    
   // always start wit(0,0)
   curX = 0;
   curY = 0;
    
   // first check if it is already out of range...
   if (curX * curX + curY * curY > (4)) {
      escapeLoop = TRUE;
   }
       
   // see how many repetitions it takes; if it takes more than
   // MAX_ITERATION, it is not part of the set. If there is already
   // proof it is a Mandelbrot Set, then the loop is exited.
   repetitionCounter = 0;
   while (repetitionCounter < 256 && !escapeLoop) {
      // store old values of x and y here, so that further calculations
      // are based on (Zn-1), aka the previous loop.
      tempCurX = curX;
      tempCurY = curY;
       
      // based on (Zn-1), the current X and Y is squared, which gives:
      // Point x = x^2 + (y^2 * i^2), where i^2 is -1.
      // Point y = 2 * x * y
      curX = tempCurX * tempCurX - (tempCurY * tempCurY);
      curY = tempCurX * tempCurY * 2;
 
      // Adds the points (cx and cy) to (curX,curY)
      curX += cx;
      curY += cy;
       
      // if the distance away is more than MAX_RANGE, then it is
      // unbounded and going to infinity; so we stop it here.
      // We calculate this using the distance formula.
      if (curX * curX + curY * curY > (4)) {
         escapeLoop = TRUE;
      }
       
      // If the curX = tempCurX, and curY = tempCurY, then it will
      // keep repeating itself, and will always reach MAX_ITERATION.
      // Therefore, we just make it jump to the loop end.
      if (curX == tempCurX && curY == tempCurY) {
         repetitionCounter = 256;
      } else {
         repetitionCounter++;
      }
   }
   return repetitionCounter;
}

double exponent(double base, double exponent) {

    double calculatedExp;
    int newExp = exponent - 1;

    if (exponent == 0){
        calculatedExp = 1;
    } else if (exponent > 0) {
        calculatedExp = base;
        while (newExp > 0){
            calculatedExp = calculatedExp * base;
            newExp--;
        }
    } else {
        calculatedExp = base;
        while (newExp < 0){
            calculatedExp = calculatedExp / base;
            newExp++;
        }
    }

    return calculatedExp;
}

double scaleCoord (double coord, int zoom) {
    double modifier = exponent(2, -zoom);
    double scaledValue = coord * modifier;

    return scaledValue;
    
}

double scale (double v, int zoom){
    return v/(exponent(2, zoom));
}