/*
*  simpleServer.c
*  1917 lab 4
*
*  Richard Buckland 28/01/11, 30/3/14.
*  Licensed under Creative Commons SA-BY-NC 3.0, share freely.
*
*/
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <netinet/in.h>
#include <unistd.h>
#include <math.h>
#include "pixelColor.h"

int waitForConnection (int serverSocket);
int makeServerSocket (int portno);
static void serveHTML (int socket);
void serveBMP (int socket, double originalX, double originalY, int zoom);
int stepsToEscape (double x, double y);
static double exponent(double base, double exponent);
double scaleCoord (double coord, int zoom);
void writeHeader (int socket);
void serveBlackBMP (int socket);
 
#define SIMPLE_SERVER_VERSION 2.0
#define REQUEST_BUFFER_SIZE 1000
#define DEFAULT_PORT 7191
#define NUMBER_OF_PAGES_TO_SERVE 100
// after serving this many pages the server will halt
 
#define BYTES_PER_PIXEL 3
#define BITS_PER_PIXEL (BYTES_PER_PIXEL*8)
#define NUMBER_PLANES 1
#define PIX_PER_METRE 2835
#define MAGIC_NUMBER 0x4d42
#define NO_COMPRESSION 0
#define OFFSET 54
#define DIB_HEADER_SIZE 40
#define NUM_COLORS 0

#define MAX_STEPS 256
#define SIZE 512

#define BYTE 1

//typedef unsigned char intensity; //to be removed later on into pixelColor.c

typedef unsigned char  bits8;
typedef unsigned short bits16;
typedef unsigned int   bits32;

typedef struct _pixel{
    char red;
    char green;
    char blue;
} pixel;

int main (int argc, char* argv[]) {
 
    printf ("************************************\n");
    printf ("Starting simple server %f\n", SIMPLE_SERVER_VERSION);
    printf ("Serving poetry since 2011\n");
    printf ("Access this server at http://localhost:%d/\n", DEFAULT_PORT);
    printf ("************************************\n");
 
    int serverSocket = makeServerSocket(DEFAULT_PORT);
    char request[REQUEST_BUFFER_SIZE];
    int numberServed = 0;

    //char URL[REQUEST_BUFFER_SIZE];
    double xVal;
    double yVal;
    int zVal;

    while (numberServed < NUMBER_OF_PAGES_TO_SERVE) {
        printf ("*** So far served %d pages ***\n", numberServed);
 
        // STEP 1. wait for a request to be sent from a web browser, 
        // then open a new connection for this conversation
        int connectionSocket = waitForConnection(serverSocket);
 
        // STEP 2. read the first line of the request
        int bytesRead = recv (connectionSocket, request, sizeof(request) - 1, 0);
        assert (bytesRead >= 0);
        // check that we were able to read some data from the connection
 
        // Get URL
        //int br = recv (connectionSocket, URL, sizeof(URL) - 1, 0);
        //assert (br >= 0);
        
        //serveHTML (connectionSocket);

        int valTote = sscanf (request, "GET /tile_x%lf_y%lf_z%d", &xVal, &yVal, &zVal);

        if (valTote == 3){

            zVal -= 7;
            xVal *= -1;
            yVal *= -1;

            //printf("X: %lf\n Y: %lf\n Z: %d\n", xVal, yVal, zVal);

            // echo entire request to the console for debugging
            printf (" *** Received http request ***\n %s\n", request);
     
            // STEP 3. send the browser a simple html page using http
            printf (" *** Sending http response ***\n");
            //serveHTML (connectionSocket);
            serveBMP (connectionSocket, xVal, yVal, zVal);
            //serveBlackBMP(connectionSocket);
        } else {
            printf("Running serveHTML\n");
            serveHTML (connectionSocket);
        }

        // STEP 4. close the connection after sending the page- keep aust beautiful
        close (connectionSocket);
        ++numberServed;
    }
 
    // close the server connection after we are done- keep aust beautiful
    printf ("** shutting down the server **\n");
    close (serverSocket);
  
    return EXIT_SUCCESS;
}

void serveBlackBMP (int socket) {
    char* message;
    // first send the http response header
    message = 
       "HTTP/1.0 200 Found\n"
       "Content-Type: image/bmp\n"
       "\n";
    printf("about to send=> %s\n", message);
    write (socket, message, strlen (message));
    // bmp header
    writeHeader (socket);

    int x = 0;
    int y = 0;
    while (y < SIZE){
        while (x < SIZE){
            write (socket, 0x00, 1);
            x++;
        }
        x = 0;
        y++;
    }
    return;
}

void serveBMP (int socket, double originalX, double originalY, int zoom) {
    char* message;
    // first send the http response header
    message = 
       "HTTP/1.0 200 Found\n"
       "Content-Type: image/bmp\n"
       "\n";
    printf("about to send=> %s\n", message);
    write (socket, message, strlen (message));

    // bmp header
    writeHeader (socket);
    // GENERATE MENDELBROT HERE

    // iterate over pixels
    // Creates a black pixel
    //pixel black = {0x00, 0x00, 0x00};

    // Declares and populates an array of pixels to be black
    //pixel pixelArray[SIZE][SIZE];

    int x = 0;
    int y = 0;
    double scaledX;
    double scaledY;
    int iterations = 0;
    pixel newPixel;


    double CxMin = -2.5;
    double CxMax = 1.5;
    double CyMin = -2.0;
    double CyMax = 2.0;

    double pWidth = (CxMax - CxMin)/512;
    double pHeight = (CyMax - CyMin)/512;

    while (y < SIZE) {
        scaledY = (((CyMin + y*pHeight))*exponent(2, -zoom)) - originalY;
        while (x < SIZE) {
            // --- THIS IS WRONG
            // scale
            scaledX = ((CxMin + x*pWidth)*exponent(2, -zoom)) - originalX;
            

            //printf("X: %d, %lf   Y: %d %lf\n", x, scaledX, y, scaledY);

            iterations = stepsToEscape (scaledX, scaledY);

            
            // Create pixel for display
            newPixel.red = stepsToRed (iterations);
            newPixel.green = stepsToGreen (iterations);
            newPixel.blue = stepsToBlue (iterations);
            
            write (socket, &newPixel.red, BYTE);
            write (socket, &newPixel.green, BYTE);
            write (socket, &newPixel.blue, BYTE);

            // Add pixel to array
            //pixelArray[y][x] = newPixel;

            x++;
        }
        x = 0;
        y++;
    }

    // Output pixels
    /*y = 0;
    x = 0;
    while (y < SIZE) {
        while (x < SIZE) {
            write (socket, (char*) pixelArray[y][x].red, BYTE);
            write (socket, (char*) pixelArray[y][x].green, BYTE);
            write (socket, (char*) pixelArray[y][x].blue, BYTE);
            x++;
        }
        x = 0;
        y++;
    }*/

}

void writeHeader (int socket) {
    assert(sizeof (bits8) == 1);
    assert(sizeof (bits16) == 2);
    assert(sizeof (bits32) == 4);

    bits16 magicNumber = MAGIC_NUMBER;
    write (socket, &magicNumber, sizeof magicNumber);

    bits32 fileSize = OFFSET + (SIZE * SIZE * BYTES_PER_PIXEL);
    write (socket, &fileSize, sizeof fileSize);

    bits32 reserved = 0;
    write (socket, &reserved, sizeof reserved);

    bits32 offset = OFFSET;
    write (socket, &offset, sizeof offset);

    bits32 dibHeaderSize = DIB_HEADER_SIZE;
    write (socket, &dibHeaderSize, sizeof dibHeaderSize);

    bits32 width = SIZE;
    write (socket, &width, sizeof width);

    bits32 height = SIZE;
    write (socket, &height, sizeof height);

    bits16 planes = NUMBER_PLANES;
    write (socket, &planes, sizeof planes);

    bits16 bitsPerPixel = BITS_PER_PIXEL;
    write (socket, &bitsPerPixel, sizeof bitsPerPixel);

    bits32 compression = NO_COMPRESSION;
    write (socket, &compression, sizeof compression);

    bits32 imageSize = (SIZE * SIZE * BYTES_PER_PIXEL);
    write (socket, &imageSize, sizeof imageSize);

    bits32 hResolution = PIX_PER_METRE;
    write (socket, &hResolution, sizeof hResolution);

    bits32 vResolution = PIX_PER_METRE;
    write (socket, &vResolution, sizeof vResolution);

    bits32 numColors = NUM_COLORS;
    write (socket, &numColors, sizeof numColors);

    bits32 importantColors = NUM_COLORS;
    write (socket, &importantColors, sizeof importantColors);
   
}

static void serveHTML (int socket) {
   char* message;
 
   // first send the http response header
   message =
      "HTTP/1.0 200 Found\n"
      "Content-Type: text/html\n"
      "\n";
   printf ("about to send=> %s\n", message);
   write (socket, message, strlen (message));
 
   message =
      "<!DOCTYPE html>\n"
      "<script src=\"http://almondbread.cse.unsw.edu.au/tiles.js\"></script>"
      "\n";      
   write (socket, message, strlen (message));
}
 
// start the server listening on the specified port number
int makeServerSocket (int portNumber) {
 
    // create socket
    int serverSocket = socket (AF_INET, SOCK_STREAM, 0);
    assert (serverSocket >= 0);
    // check there was no error in opening the socket
 
    // bind the socket to the listening port  (7191 in this case)
    struct sockaddr_in serverAddress;
    serverAddress.sin_family      = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port        = htons (portNumber);
 
    // tell the server to restart immediately after a previous shutdown
    // even if it looks like the socket is still in use
    // otherwise we might have to wait a little while before rerunning the
    // server once it has stopped
    const int optionValue = 1;
    setsockopt (serverSocket, SOL_SOCKET, SO_REUSEADDR, &optionValue, sizeof (int));
 
    int bindSuccess = bind (serverSocket, (struct sockaddr*)&serverAddress, sizeof (serverAddress));
 
    assert (bindSuccess >= 0);
    // if this assert fails wait a short while to let the operating
    // system clear the port before trying again
 
    return serverSocket;
}
 
// wait for a browser to request a connection,
// returns the socket on which the conversation will take place
int waitForConnection (int serverSocket) {
 
    // listen for a connection
    const int serverMaxBacklog = 10;
    listen (serverSocket, serverMaxBacklog);
 
    // accept the connection
    struct sockaddr_in clientAddress;
    socklen_t clientLen = sizeof (clientAddress);
    int connectionSocket = accept (serverSocket, (struct sockaddr*)&clientAddress, &clientLen);
    assert (connectionSocket >= 0);
    // check for connection error
 
    return connectionSocket;
}


int stepsToEscape (double x, double y) {
    int iterations = 0;
    //The c values are the co-ordinates being tested
    double cX = x;
    double cY = y;
    //Sets the 'old' or Zn values to the initial co-ordinates
    double oldX = 0;
    double oldY = 0;
    //The values gained from each iteration, to be set into oldX/Y afterwards.
    double newX = 0;
    double newY = 0;
    //While not escaped or complete max interations
    while (newX*newX + newY*newY < 4 && iterations <= MAX_STEPS) {
        //printf ("Current Z coordinate value: (%lf, %lf)\n", oldX, oldY);
        //Iterate the mandelbrot equation for both x and y

        //Complex squares - real parts = x^2 - y^2
        newX = oldX*oldX - oldY*oldY + cX;

        //imag parts = 2xy
        newY = 2*oldX*oldY + cY;

        
        oldX = newX;
        oldY = newY;

        iterations++;
    } 
    //printf ("Final Z coordinate value: (%lf, %lf)\n", oldX, oldY);
    //printf ("Num iterations: %d\n", iterations);

    return iterations; 
}
 
/*double exponent(double base, double exponent) {

    double calculatedExp;
    int newExp = exponent - 1;

    if (exponent == 0){
        calculatedExp = 1;
    } else if (exponent > 0) {
        calculatedExp = base;
        while (newExp > 0){
            calculatedExp = calculatedExp * base;
            newExp--;
        }
    } else {
        calculatedExp = base;
        while (newExp < 0){
            calculatedExp = calculatedExp / base;
            newExp++;
        }
    }

    return calculatedExp;
}
*/
double scaleCoord (double coord, int zoom) {
    double modifier = exponent(2, -zoom);
    double scaledValue = coord * modifier;

    return scaledValue;
    
}

/*//To be removed into pixelColor.c later on
unsigned char stepsToRed (int steps) {
    intensity red = steps;
    return red;
}
unsigned char stepsToBlue (int steps) {
    intensity blue;
    if (steps <= 5 || steps >= 240){
        blue = 0;
    } else {
        blue = 255 - steps;
    }
    return blue;
}
unsigned char stepsToGreen (int steps) {
    intensity green;
    if (steps <= 15 || steps >= 240){
        green = 0;
    } else {
        green = 255 - steps;
    }
    return green;
}
*/

int factorial (int k);
double sine (double x);
static double exponent(double base, double exponent);

typedef unsigned char intensity;
//Need to subtract 1 from steps (1 to 256) to make it 0 to 255
unsigned char stepsToRed (int steps) {
    intensity red;
    if (steps % 3 == 0){
        red = 255;
    } else if (steps % 3 == 1){
        red = sine(steps/100) * 50;
    } else {
        red = 0;
    }

    if (steps >= 255){
        red = 0;
    }

    return red;
}
unsigned char stepsToBlue (int steps) {
    intensity blue;
    if (steps % 3 == 0){
        blue = 0;
    } else if (steps % 3 == 1){
        blue = (steps/100) * 50;
    } else {
        blue = 255;
    }

    if (steps >= 255){
        blue = 0;
    }

    return blue;
}
unsigned char stepsToGreen (int steps) {
    intensity green;
    if (steps % 3 == 0){
        green = 0;
    } else if (steps % 3 == 1){
        green = factorial((int)(steps/100));
    } else {
        green = 0;
    }

    if (steps >= 255){
        green = 0;
    }

    return green;
}

int factorial (int k){

    int returnValue;

    if (k == 0){
        returnValue = 1;
    } else {
        returnValue = (k * factorial (k - 1));
    }

    return returnValue;
}

double sine (double x) {
    //Calling the term that is x's exponent and factorial denominator 'coeff'
    int coeff = 0;
    //int counter = 0;
    double sequenceSum = 0;
    while (coeff < 10) {
        sequenceSum += (exponent (-1, coeff) / factorial (2 * coeff + 1)) * exponent (x, 2 * coeff + 1);
        //printf ("%lf\n", sequenceSum);
        coeff++;
    }

    return sequenceSum;

}

static double exponent(double base, double exponent) {

    double calculatedExp;
    int newExp = exponent - 1;

    if (exponent == 0){
        calculatedExp = 1;
    } else if (exponent > 0) {
        calculatedExp = base;
        while (newExp > 0){
            calculatedExp = calculatedExp * base;
            newExp--;
        }
    } else {
        calculatedExp = base;
        while (newExp < 0){
            calculatedExp = calculatedExp / base;
            newExp++;
        }
    }

    return calculatedExp;
}

/*
this code calls these external networking functions
try to work out what they do from seeing how they are used,
then google them for full details. 
 
recv
close
send
socket
setsockopt
bind
listen
accept
*/
