#include <stdio.h>
#include <stdlib.h>

double scaleCoord (double coord, int zoom);
double exponent (double base, double exponent);


int main (int argc, char *argv[]){
    double originalX = 1;
    double originalY = 1;
    int zoom = 2;
    double scaledX = scaleCoord (originalX, zoom);
    double scaledY = scaleCoord (originalY, zoom);
    printf("Original: (%lf, %lf), Scaled: (%lf, %lf)\n", originalX, originalY, scaledX, scaledY);
    return EXIT_SUCCESS;
}

double scaleCoord (double coord, int zoom){
    double modifier = exponent(2, -zoom);
    double scaledValue = coord * modifier;

    return scaledValue;
    
}

double exponent (double base, double exponent){

    double calculatedExp;
    int newExp = exponent - 1;

    if (exponent == 0){
        calculatedExp = 1;
    } else if (exponent > 0) {
        calculatedExp = base;
        while (newExp > 0){
            calculatedExp = calculatedExp * base;
            newExp--;
        }
    } else {
        calculatedExp = base;
        while (newExp < 0){
            calculatedExp = calculatedExp / base;
            newExp++;
        }
    }

    return calculatedExp;
}