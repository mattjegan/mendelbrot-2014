#include "pixelColor.h"
#include <math.h>

int factorial (int k);
double sine (double x);
double exponent(double base, double exponent);

#define MAX_HUE 255
#define PERCENT 100

typedef unsigned char intensity;
//Need to subtract 1 from steps (1 to 256) to make it 0 to 255
unsigned char stepsToRed (int steps) {
	intensity red = sine(steps/PERCENT) * MAX_HUE;
	return red;
}
unsigned char stepsToBlue (int steps) {
	intensity blue = cos(steps/PERCENT) * MAX_HUE;
	return blue;
}
unsigned char stepsToGreen (int steps) {
	intensity green = MAX_HUE - steps;
	return green;
}

int factorial (int k){

    int returnValue;

    if (k == 0){
        returnValue = 1;
    } else {
        returnValue = (k * factorial (k - 1));
    }

    return returnValue;
}

double sine (double x) {
    //Calling the term that is x's exponent and factorial denominator 'coeff'
    int coeff = 0;
    //int counter = 0;
    double sequenceSum = 0;
    //Do 10 runs, yields answer for small numbers
    while (coeff < 10) {
        //This is the taylor series
        sequenceSum += (exponent (-1, coeff) / factorial (2 * coeff + 1)) * exponent (x, 2 * coeff + 1);
        //printf ("%lf\n", sequenceSum);
        coeff++;
    }

    return sequenceSum;

}

double exponent(double base, double exponent) {

    double calculatedExp;
    int newExp = exponent - 1;

    if (exponent == 0){
        calculatedExp = 1;
    } else if (exponent > 0) {
        calculatedExp = base;
        while (newExp > 0){
            calculatedExp = calculatedExp * base;
            newExp--;
        }
    } else {
        calculatedExp = base;
        while (newExp < 0){
            calculatedExp = calculatedExp / base;
            newExp++;
        }
    }

    return calculatedExp;
}