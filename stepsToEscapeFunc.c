/*
Designing the function to return how
many iterations it takes for a point to escape
By Joe Harris
13/04/14
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#define MAX_STEPS 255

int stepsToEscape (double x, double y);

int main (int argc, char *argv[]) {
    
    double x = 0;
    double y;
    
    printf ("Please enter an X co-ord: \n");
    scanf ("%lf", &x);
    printf ("Please enter a Y co-ord: \n");
    scanf ("%lf", &y);

    printf ("Took %d iteration(s) to escape. \n\n", stepsToEscape (x, y));
    

    //unit testing (passes all current tests)
    //testEscapeSteps();

    return EXIT_SUCCESS;
}

int stepsToEscape (double x, double y) {
    int iterations = 0;
    //The c values are the co-ordinates being tested
    double cX = x;
    double cY = y;
    //Sets the 'old' or Zn values to the initial co-ordinates
    double oldX = 0;
    double oldY = 0;
    //The values gained from each iteration, to be set into oldX/Y afterwards.
    double newX = 0;
    double newY = 0;
    //While not escaped or complete max interations
    while (newX*newX + newY*newY < 4 && iterations <= MAX_STEPS) {
	    printf ("Current Z coordinate value: (%lf, %lf)\n", oldX, oldY);
    	//Iterate the mandelbrot equation for both x and y

        //Complex squares - real parts = x^2 - y^2
        newX = oldX*oldX - oldY*oldY + cX;

        //imag parts = 2xy
        newY = 2*oldX*oldY + cY;

        
        oldX = newX;
        oldY = newY;

        iterations++;
    } 
    printf ("Final Z coordinate value: (%lf, %lf)\n", oldX, oldY);
    //printf ("Num iterations: %d\n", iterations);

    return iterations; 
}
