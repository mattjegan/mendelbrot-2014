/*
*  mandelbrot.c
*  COMP1917 Task 2
*
*  Joseph Harris (z5016148) and Matthew Egan (z3424645)
*  Friday 12pm Tuba - Sarah Bennett
* 
*  Original server code by Richard Buckland 28/01/11, 30/3/14.
*  Licensed under Creative Commons SA-BY-NC 3.0, share freely.
*
*  A server that serves a 512x512 image of the mandelbrot set
*  given the x and y offsets as well as the zoom.
*  If none given, a javascript viewer is used.
*
*/
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <netinet/in.h>
#include <unistd.h>
#include <math.h>
#include "pixelColor.h"
#include "mandelbrot.h"

// Server Functions
int waitForConnection (int serverSocket);
int makeServerSocket (int portno);
void serveHTML (int socket);

// Mandelbrot Functions
void serveBMP (int socket, double originalX, double originalY,
    int zoom);
void writeHeader (int socket);
static double exponent(double base, double exponent);
//static void testEscapeSteps(void);
 
#define SIMPLE_SERVER_VERSION 2.0
#define REQUEST_BUFFER_SIZE 1000
#define DEFAULT_PORT 7191
#define NUMBER_OF_PAGES_TO_SERVE 100
// after serving this many pages the server will halt
 
#define BYTES_PER_PIXEL 3
#define BITS_PER_PIXEL (BYTES_PER_PIXEL*8)
#define NUMBER_PLANES 1
#define PIX_PER_METRE 2835
#define MAGIC_NUMBER 0x4d42
#define NO_COMPRESSION 0
#define OFFSET 54
#define DIB_HEADER_SIZE 40
#define NUM_COLORS 0

#define MAX_STEPS 256
#define SIZE 512

#define BYTE 1

#define NUM_VALUES 3
#define ZOOM_SHIFT 7
#define INVERT -1

#define ZOOM_BASE 2

typedef unsigned char  bits8;
typedef unsigned short bits16;
typedef unsigned int   bits32;

//Struct to contain the RGB values of pixels to be written to the BMP
typedef struct _pixel{
    char red;
    char green;
    char blue;
} pixel;

int main (int argc, char* argv[]) {
    //testEscapeSteps();
    printf ("************************************\n");
    printf ("Starting simple server %f\n", SIMPLE_SERVER_VERSION);
    printf ("Serving poetry since 2011\n");
    printf ("Access this server at http://localhost:%d/\n",
        DEFAULT_PORT);
    printf ("************************************\n");
 
    int serverSocket = makeServerSocket(DEFAULT_PORT);
    char request[REQUEST_BUFFER_SIZE];
    int numberServed = 0;
    // Variables to hold x,y,z values from URL
    double xVal;
    double yVal;
    int zVal;

    while (numberServed < NUMBER_OF_PAGES_TO_SERVE) {
        printf ("*** So far served %d pages ***\n", numberServed);
 
        // STEP 1. wait for a request to be sent from a web browser, 
        // then open a new connection for this conversation
        int connectionSocket = waitForConnection(serverSocket);
 
        // STEP 2. read the first line of the request
        int bytesRead = recv (connectionSocket, request,
            sizeof(request) - 1, 0);
        // check that we were able to read some data
        // from the connection
        assert (bytesRead >= 0);
        //valtote is the number of values that sscanf managed to read
        int valTote = sscanf (request, "GET /tile_x%lf_y%lf_z%d",
            &xVal, &yVal, &zVal);
        //If all of the values are read then serve the BMP image
        if (valTote == NUM_VALUES) {
            
            zVal -= ZOOM_SHIFT;
            xVal *= INVERT;
            yVal *= INVERT;

            // echo entire request to the console for debugging
            printf (" *** Received http request ***\n %s\n", request);
     
            // STEP 3. send the browser a simple html page using http
            printf (" *** Sending http response ***\n");
            serveBMP (connectionSocket, xVal, yVal, zVal);
        } else {
            // If there are not 3 values (x,y,z) to be read then serve
            // the HTML viewer instead of the image
            printf("Running serveHTML\n");
            serveHTML (connectionSocket);
        }

        // STEP 4. close the connection after sending the page- 
        // keep aust beautiful
        close (connectionSocket);
        ++numberServed;
    }
 
    // close the server connection after we are done-
    // keep aust beautiful
    printf ("** shutting down the server **\n");
    close (serverSocket);
  
    return EXIT_SUCCESS;
}

void serveBMP (int socket, double originalX, double originalY, 
        int zoom) {
    char* message;
    // first send the http response message
    message = 
       "HTTP/1.0 200 Found\n"
       "Content-Type: image/bmp\n"
       "\n";
    printf("about to send=> %s\n", message);
    write (socket, message, strlen (message));

    // Write BMP Header
    writeHeader (socket);

    int x = 0;
    int y = 0;
    double scaledX;
    double scaledY;
    int iterations = 0;
    pixel newPixel;

    double CxMin = -2.5;
    double CxMax = 1.5;
    double CyMin = -2.0;
    double CyMax = 2.0;

    double pWidth = (CxMax - CxMin)/512;
    double pHeight = (CyMax - CyMin)/512;
    // Nested while loops to iterate through the x & y of the BMP
    while (y < SIZE) {
        scaledY = (((CyMin + y*pHeight)) * exponent(2, -zoom))
            - originalY;
        while (x < SIZE) {
            scaledX = ((CxMin + x*pWidth) * exponent(2, -zoom)) 
                - originalX;
            
            iterations = escapeSteps (scaledX, scaledY);
            
            // Create pixel for display
            newPixel.red = stepsToRed (iterations);
            newPixel.green = stepsToGreen (iterations);
            newPixel.blue = stepsToBlue (iterations);
            // Write each color to the server
            write (socket, &newPixel.red, BYTE);
            write (socket, &newPixel.green, BYTE);
            write (socket, &newPixel.blue, BYTE);

            x++;
        }
        x = 0;
        y++;
    }
}

void writeHeader (int socket) {
    assert(sizeof (bits8) == 1);
    assert(sizeof (bits16) == 2);
    assert(sizeof (bits32) == 4);

    bits16 magicNumber = MAGIC_NUMBER;
    write (socket, &magicNumber, sizeof magicNumber);

    bits32 fileSize = OFFSET + (SIZE * SIZE * BYTES_PER_PIXEL);
    write (socket, &fileSize, sizeof fileSize);

    bits32 reserved = 0;
    write (socket, &reserved, sizeof reserved);

    bits32 offset = OFFSET;
    write (socket, &offset, sizeof offset);

    bits32 dibHeaderSize = DIB_HEADER_SIZE;
    write (socket, &dibHeaderSize, sizeof dibHeaderSize);

    bits32 width = SIZE;
    write (socket, &width, sizeof width);

    bits32 height = SIZE;
    write (socket, &height, sizeof height);

    bits16 planes = NUMBER_PLANES;
    write (socket, &planes, sizeof planes);

    bits16 bitsPerPixel = BITS_PER_PIXEL;
    write (socket, &bitsPerPixel, sizeof bitsPerPixel);

    bits32 compression = NO_COMPRESSION;
    write (socket, &compression, sizeof compression);

    bits32 imageSize = (SIZE * SIZE * BYTES_PER_PIXEL);
    write (socket, &imageSize, sizeof imageSize);

    bits32 hResolution = PIX_PER_METRE;
    write (socket, &hResolution, sizeof hResolution);

    bits32 vResolution = PIX_PER_METRE;
    write (socket, &vResolution, sizeof vResolution);

    bits32 numColors = NUM_COLORS;
    write (socket, &numColors, sizeof numColors);

    bits32 importantColors = NUM_COLORS;
    write (socket, &importantColors, sizeof importantColors);
}

void serveHTML (int socket) {
   char* message;
 
   // first send the http response header
   message =
      "HTTP/1.0 200 Found\n"
      "Content-Type: text/html\n"
      "\n";
   printf ("about to send=> %s\n", message);
   write (socket, message, strlen (message));
 
   message =
      "<!DOCTYPE html>\n"
      "<script src=\"http://almondbread.cse.unsw.edu.au/tiles.js\">\
      </script>"
      "\n";      
   write (socket, message, strlen (message));
}
 
// start the server listening on the specified port number
int makeServerSocket (int portNumber) {
 
    // create socket
    int serverSocket = socket (AF_INET, SOCK_STREAM, 0);
    assert (serverSocket >= 0);
    // check there was no error in opening the socket
 
    // bind the socket to the listening port  (7191 in this case)
    struct sockaddr_in serverAddress;
    serverAddress.sin_family      = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port        = htons (portNumber);
 
    // tell the server to restart immediately after a previous shutdown
    // even if it looks like the socket is still in use otherwise we
    //  might have to wait a little while before rerunning the server
    // once it has stopped
    const int optionValue = 1;
    setsockopt (serverSocket, SOL_SOCKET, SO_REUSEADDR, 
        &optionValue, sizeof (int));
 
    int bindSuccess = bind (serverSocket, 
        (struct sockaddr*)&serverAddress, sizeof (serverAddress));
 
    assert (bindSuccess >= 0);
    // if this assert fails wait a short while to let the operating
    // system clear the port before trying again
 
    return serverSocket;
}
 
// wait for a browser to request a connection,
// returns the socket on which the conversation will take place
int waitForConnection (int serverSocket) {
 
    // listen for a connection
    const int serverMaxBacklog = 10;
    listen (serverSocket, serverMaxBacklog);
 
    // accept the connection
    struct sockaddr_in clientAddress;
    socklen_t clientLen = sizeof (clientAddress);
    int connectionSocket = accept (serverSocket, 
        (struct sockaddr*)&clientAddress, &clientLen);
    assert (connectionSocket >= 0);
    // check for connection error
 
    return connectionSocket;
}


int escapeSteps (double x, double y) {
    int iterations = 0;

    // The c values are the co-ordinates being tested
    double cX = x;
    double cY = y;

    // Sets the 'old' or Zn values to the initial co-ordinates
    double oldX = 0;
    double oldY = 0;

    // The values gained from each iteration,
    // to be set into oldX/Y afterwards.
    double newX = 0;
    double newY = 0;

    // While not escaped or completed max interations
    while (newX*newX + newY*newY < 4 && iterations <= MAX_STEPS) {
        // Iterate the mandelbrot equation for both x and y

        // Complex squares: real parts = x^2 - y^2
        newX = oldX*oldX - oldY*oldY + cX;

        // imag parts = 2xy
        newY = 2*oldX*oldY + cY;

        oldX = newX;
        oldY = newY;

        iterations++;
    } 
    return iterations; 
}

static double exponent(double base, double exponent) {
    double calculatedExp;
    int newExp = exponent - 1;
    
    if (exponent == 0){
        calculatedExp = 1;
    } else if (exponent > 0) {
        calculatedExp = base;
        while (newExp > 0){
            calculatedExp = calculatedExp * base;
            newExp--;
        }
    } else {
        calculatedExp = base;
        while (newExp < 0){
            calculatedExp = calculatedExp / base;
            newExp++;
        }
    }
    return calculatedExp;
}
/*
static void testEscapeSteps(void) {
   printf (" Testing escapeTests ...\n");
   printf (
     "  (using a few sample unit tests only - the automarker "
     "will use more tests)\n"
   );
 
   assert (escapeSteps (100.0, 100.0) == 1);
   assert (escapeSteps (0.0, 0.0)     == 256);
 
   assert (escapeSteps (-1.5000000000000, -1.5000000000000) == 1);
   assert (escapeSteps (-1.4250000000000, -1.4250000000000) == 1);
   assert (escapeSteps (-1.3500000000000, -1.3500000000000) == 2);
   assert (escapeSteps (-1.2750000000000, -1.2750000000000) == 2);
   assert (escapeSteps (-1.2000000000000, -1.2000000000000) == 2);
   assert (escapeSteps (-1.1250000000000, -1.1250000000000) == 3);
   assert (escapeSteps (-1.0500000000000, -1.0500000000000) == 3);
   assert (escapeSteps (-0.9750000000000, -0.9750000000000) == 3);
   assert (escapeSteps (-0.9000000000000, -0.9000000000000) == 3);
   assert (escapeSteps (-0.8250000000000, -0.8250000000000) == 4);
   assert (escapeSteps (-0.7500000000000, -0.7500000000000) == 4);
   assert (escapeSteps (-0.6750000000000, -0.6750000000000) == 6);
   assert (escapeSteps (-0.6000000000000, -0.6000000000000) == 12);
   assert (escapeSteps (-0.5250000000000, -0.5250000000000) == 157);
   assert (escapeSteps (-0.4500000000000, -0.4500000000000) == 256);
   assert (escapeSteps (-0.3750000000000, -0.3750000000000) == 256);
   assert (escapeSteps (-0.3000000000000, -0.3000000000000) == 256);
   assert (escapeSteps (-0.2250000000000, -0.2250000000000) == 256);
   assert (escapeSteps (-0.1500000000000, -0.1500000000000) == 256);
   assert (escapeSteps (-0.0750000000000, -0.0750000000000) == 256);
   assert (escapeSteps (-0.0000000000000, -0.0000000000000) == 256);
 
   assert (escapeSteps (-0.5400000000000, 0.5600000000000) == 256);
   assert (escapeSteps (-0.5475000000000, 0.5650000000000) == 58);
   assert (escapeSteps (-0.5550000000000, 0.5700000000000) == 28);
   assert (escapeSteps (-0.5625000000000, 0.5750000000000) == 22);
   assert (escapeSteps (-0.5700000000000, 0.5800000000000) == 20);
   assert (escapeSteps (-0.5775000000000, 0.5850000000000) == 15);
   assert (escapeSteps (-0.5850000000000, 0.5900000000000) == 13);
   assert (escapeSteps (-0.5925000000000, 0.5950000000000) == 12);
   assert (escapeSteps (-0.6000000000000, 0.6000000000000) == 12);
 
   assert (escapeSteps (0.2283000000000, -0.5566000000000) == 20);
   assert (escapeSteps (0.2272500000000, -0.5545000000000) == 19);
   assert (escapeSteps (0.2262000000000, -0.5524000000000) == 19);
   assert (escapeSteps (0.2251500000000, -0.5503000000000) == 20);
   assert (escapeSteps (0.2241000000000, -0.5482000000000) == 20);
   assert (escapeSteps (0.2230500000000, -0.5461000000000) == 21);
   assert (escapeSteps (0.2220000000000, -0.5440000000000) == 22);
   assert (escapeSteps (0.2209500000000, -0.5419000000000) == 23);
   assert (escapeSteps (0.2199000000000, -0.5398000000000) == 26);
   assert (escapeSteps (0.2188500000000, -0.5377000000000) == 256);
   assert (escapeSteps (0.2178000000000, -0.5356000000000) == 91);
   assert (escapeSteps (0.2167500000000, -0.5335000000000) == 256);
 
   assert (escapeSteps (-0.5441250000000, 0.5627500000000) == 119);
   assert (escapeSteps (-0.5445000000000, 0.5630000000000) == 88);
   assert (escapeSteps (-0.5448750000000, 0.5632500000000) == 83);
   assert (escapeSteps (-0.5452500000000, 0.5635000000000) == 86);
   assert (escapeSteps (-0.5456250000000, 0.5637500000000) == 74);
   assert (escapeSteps (-0.5460000000000, 0.5640000000000) == 73);
   assert (escapeSteps (-0.5463750000000, 0.5642500000000) == 125);
   assert (escapeSteps (-0.5467500000000, 0.5645000000000) == 75);
   assert (escapeSteps (-0.5471250000000, 0.5647500000000) == 60);
   assert (escapeSteps (-0.5475000000000, 0.5650000000000) == 58);
 
   assert (escapeSteps (0.2525812510000, 0.0000004051626) == 60);
   assert (escapeSteps (0.2524546884500, 0.0000004049095) == 61);
   assert (escapeSteps (0.2523281259000, 0.0000004046564) == 63);
   assert (escapeSteps (0.2522015633500, 0.0000004044033) == 65);
   assert (escapeSteps (0.2520750008000, 0.0000004041502) == 67);
   assert (escapeSteps (0.2519484382500, 0.0000004038971) == 69);
   assert (escapeSteps (0.2518218757000, 0.0000004036441) == 72);
   assert (escapeSteps (0.2516953131500, 0.0000004033910) == 74);
   assert (escapeSteps (0.2515687506000, 0.0000004031379) == 77);
   assert (escapeSteps (0.2514421880500, 0.0000004028848) == 81);
   assert (escapeSteps (0.2513156255000, 0.0000004026317) == 85);
   assert (escapeSteps (0.2511890629500, 0.0000004023786) == 89);
   assert (escapeSteps (0.2510625004000, 0.0000004021255) == 94);
   assert (escapeSteps (0.2509359378500, 0.0000004018724) == 101);
   assert (escapeSteps (0.2508093753000, 0.0000004016193) == 108);
   assert (escapeSteps (0.2506828127500, 0.0000004013662) == 118);
   assert (escapeSteps (0.2505562502000, 0.0000004011132) == 131);
   assert (escapeSteps (0.2504296876500, 0.0000004008601) == 150);
   assert (escapeSteps (0.2503031251000, 0.0000004006070) == 179);
   assert (escapeSteps (0.2501765625500, 0.0000004003539) == 235);
   assert (escapeSteps (0.2500500000000, 0.0000004001008) == 256);
 
   assert (escapeSteps (0.3565670191423, 0.1094322101123) == 254);
   assert (escapeSteps (0.3565670191416, 0.1094322101120) == 255);
   assert (escapeSteps (0.3565670191409, 0.1094322101118) == 256);
   assert (escapeSteps (0.3565670950000, 0.1094322330000) == 222);
   assert (escapeSteps (0.3565670912300, 0.1094322318625) == 222);
   assert (escapeSteps (0.3565670874600, 0.1094322307250) == 222);
   assert (escapeSteps (0.3565670836900, 0.1094322295875) == 222);
   assert (escapeSteps (0.3565670799200, 0.1094322284500) == 222);
   assert (escapeSteps (0.3565670761500, 0.1094322273125) == 222);
   assert (escapeSteps (0.3565670723800, 0.1094322261750) == 222);
   assert (escapeSteps (0.3565670686100, 0.1094322250375) == 223);
   assert (escapeSteps (0.3565670648400, 0.1094322239000) == 223);
   assert (escapeSteps (0.3565670610700, 0.1094322227625) == 224);
   assert (escapeSteps (0.3565670573000, 0.1094322216250) == 225);
   assert (escapeSteps (0.3565670535300, 0.1094322204875) == 256);
   assert (escapeSteps (0.3565670497600, 0.1094322193500) == 256);
   assert (escapeSteps (0.3565670459900, 0.1094322182125) == 237);
   assert (escapeSteps (0.3565670422200, 0.1094322170750) == 233);
   assert (escapeSteps (0.3565670384500, 0.1094322159375) == 232);
   assert (escapeSteps (0.3565670346800, 0.1094322148000) == 232);
   assert (escapeSteps (0.3565670309100, 0.1094322136625) == 232);
   assert (escapeSteps (0.3565670271400, 0.1094322125250) == 233);
   assert (escapeSteps (0.3565670233700, 0.1094322113875) == 234);
   assert (escapeSteps (0.3565670196000, 0.1094322102500) == 243);
 }
 */
