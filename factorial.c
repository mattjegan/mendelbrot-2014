int factorial (int n){

	int returnValue;

	if (n == 0){
		returnValue = 1;
	} else {
		returnValue = (n * factorial (n - 1));
	}

	return returnValue;
}