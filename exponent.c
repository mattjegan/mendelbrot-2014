#include <stdio.h>
#include <stdlib.h>

double exponent(double base, double exponent);

int main(int argc, char *argv[]){
    printf("%lf\n", exponent(2,8));
    return EXIT_SUCCESS;
}

double exponent(double base, double exponent){

    double calculatedExp;
    int newExp = exponent - 1;

    if (exponent == 0){
        calculatedExp = 1;
    } else if (exponent > 0) {
        calculatedExp = base;
        while (newExp > 0){
            calculatedExp = calculatedExp * base;
            newExp--;
        }
    } else {
        calculatedExp = base;
        while (newExp < 0){
            calculatedExp = calculatedExp / base;
            newExp++;
        }
    }

    return calculatedExp;
}