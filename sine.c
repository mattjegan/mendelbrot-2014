#include <stdio.h>
#include <stdlib.h>

int factorial (int k);
double sine (double x);
double exponent(double base, double exponent);

int main (int argc, char *argv[]){
    for (double i = 0; i <= 1.5; i += 0.001) {
        printf ("sin(%lf) is %lf\n", i, sine (i));
        printf ("RGB = %lf\n", 255*sine(i));
    }
    //double x;
    //printf ("Please enter an x for sin(x): \n");
    //scanf ("%lf", &x);
    
    return EXIT_SUCCESS;
}


int factorial (int k){

    int returnValue;

    if (k == 0){
        returnValue = 1;
    } else {
        returnValue = (k * factorial (k - 1));
    }

    return returnValue;
}

double sine (double x) {
    //Calling the term that is x's exponent and factorial denominator 'coeff'
    int coeff = 0;
    //int counter = 0;
    double sequenceSum = 0;
    while (coeff < 10) {
        sequenceSum += (exponent (-1, coeff) / factorial (2 * coeff + 1)) * exponent (x, 2 * coeff + 1);
        //printf ("%lf\n", sequenceSum);
        coeff++;
    }

    return sequenceSum;

}

double exponent(double base, double exponent) {

    double calculatedExp;
    int newExp = exponent - 1;

    if (exponent == 0){
        calculatedExp = 1;
    } else if (exponent > 0) {
        calculatedExp = base;
        while (newExp > 0){
            calculatedExp = calculatedExp * base;
            newExp--;
        }
    } else {
        calculatedExp = base;
        while (newExp < 0){
            calculatedExp = calculatedExp / base;
            newExp++;
        }
    }

    return calculatedExp;
}