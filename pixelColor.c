#include "pixelColor.h"
#include <math.h>

// Homemade sin(x) function; initially couldn't use math.h so had to 
// research Taylor/Power Series and recreate sin, was going to do
// e^x, cos(x) and tan(x) but Richard kindly unbanned math.h.
// Also homemade factorial and 'to-the-power-of' function
static int factorial (int k); 
static double sine (double x);
static double exponent(double base, double exponent);

typedef unsigned char intensity;

unsigned char stepsToRed (int steps) {
    intensity red;
    if (steps % 3 == 0){
        red = 255;
    } else if (steps % 3 == 1){
        red = sine(steps/100) * 50;
    } else {
        red = 0;
    }

    if (steps >= 255){
        red = 0;
    }

    return red;
}

unsigned char stepsToBlue (int steps) {
    intensity blue;
    if (steps % 3 == 0){
        blue = 0;
    } else if (steps % 3 == 1){
        blue = (steps/100) * 50;
    } else {
        blue = 255;
    }

    if (steps >= 255){
        blue = 0;
    }

    return blue;
}

unsigned char stepsToGreen (int steps) {
    intensity green;
    if (steps % 3 == 0){
        green = 0;
    } else if (steps % 3 == 1){
        green = factorial((int)(steps/100));
    } else {
        green = 0;
    }

    if (steps >= 255){
        green = 0;
    }

    return green;
}

static int factorial (int k){
    int returnValue;
    // Using recursion get the factorial of k
    if (k == 0){
        returnValue = 1;
    } else {
        returnValue = (k * factorial (k - 1));
    }

    return returnValue;
}

static double sine (double x) {
    // term that is x's exponent and factorial denominator 'coeff'
    int coeff = 0;
    // int counter = 0;
    double sequenceSum = 0;
    // In actual fact the taylor series is an infinite sum, this is not
    // really feasible so through some thinking and testing decided 
    // that as only small values need to be tested, 10 passes should 
    // be enough to get an accurate enough estimation with minimal 
    // strain on the system (really fast calculation time).
    while (coeff < 10) {
        // While looks complex, it is in fact the Taylor Series for
        // sin(x). Very high level mathematics, better to abstract
        // away the complexity and just accept that it works.
        sequenceSum += (exponent (-1, coeff) / 
            factorial (2 * coeff + 1)) * exponent (x, 2 * coeff + 1);
        coeff++;
    }

    return sequenceSum;

}

static double exponent(double base, double exponent) {

    double calculatedExp;
    int newExp = exponent - 1;
    
    if (exponent == 0){
        calculatedExp = 1;
    } else if (exponent > 0) {
        calculatedExp = base;
        while (newExp > 0){
            calculatedExp = calculatedExp * base;
            newExp--;
        }
    } else {
        calculatedExp = base;
        while (newExp < 0){
            calculatedExp = calculatedExp / base;
            newExp++;
        }
    }

    return calculatedExp;
}
