#define MAX_STEPS 256
#define SIZE 512

typedef struct _pixel{
    char red;
    char green;
    char blue;
} pixel;

void mendel(){
    // Recieve any inputs needed - zoom, x and y offset etc...

    // Creates a black pixel
    pixel black = {0x00, 0x00, 0x00};

    // Declares and populates an array of pixels to be black
    pixel pixelArray[SIZE][SIZE] = {black};

    // Required variables
    int iteration = 0;
    int x = 0;
    int y = 0;
    int nx = pow(x, 2) + x;
    int ny = pow(y, 2) + y;

    int cx = x;
    int cy = y;

    // Loops through the rows in the array
    while (y < SIZE){
        // Loops through each column in a row
        while (x < SIZE){
            // Iterates over a point until a sqrt(x^2 + y^2) >= 4 or escapes the mendelbrot sequence
            while ((nx*nx + ny*ny) < 4 || iteration <= MAX_STEPS){
                // Mendelbrot equations
                // x and y are used to define zoom and offset, just not implemented in this example
                nx = pow(nx, 2) + cx;
                ny = pow(ny, 2) + cy;
                iteration++;
            }
            // Creates a pixel based on the iterations
            pixel new = convertColors(iteration);

            // Puts the new pixel in the array
            pixelArray[y][x] = new;

            // Continues through the points
            iteration = 0;
            x++;
        }
        y++;
    }

    // Write the array to an image
}

pixel convertColors(){
    return somePixel;
}
